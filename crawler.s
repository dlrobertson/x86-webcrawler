.section .data
urlfmt:
    .asciz "%s\n"
chfmt:
    .asciz "%c\n"
ptrfmt:
    .asciz "%p\n"
retfmt:
    .asciz "%d\n"
starturl:
    .string "http://www.google.com"

.section .text
.global main


.writecb:
    pushq %rbp
    movq %rsp, %rbp
    subq $0x18, %rsp
    movq %rdi, -0x10(%rsp)
    movq $0x1, -0x8(%rsp)
    movq %rdi, (%rsp)
    leaq -1(%rdi, %rdx), %r9

    jmp .findbase
.findmid:
    movq -0x10(%rsp), %rdi
    # callq .findhrefs will find hrefs
    addq $0x3, -0x10(%rsp) # jumps at least three at a time to find "<a "
    addq $0x1, -0x8(%rsp)
.findbase:
    cmpq %r9, -0x10(%rsp)
    jb .findmid

    # prints the number of characters found
    movq -0x8(%rsp), %rsi
    movq $retfmt, %rdi
    xorq %rax, %rax
    callq printf

    leave
    rep; retq


.startcrawler:
    pushq %rbp
    movq %rsp, %rbp
    subq $0x8, %rsp
    pushq %rdi

    movq $starturl, %rsi # %rdi is already set
    callq .curlfunc

    movq (%rsp), %rax
    leave
    rep; retq

.curlfunc:
    pushq %rbp
    movq %rsp, %rbp
    subq $0x10, %rsp
    pushq %rsi
    pushq %rdi

    # URL
    movq $starturl, %rdx 
    movq (%rsp), %rdi
    movq $10002, %rsi # %rdi is already set
    xorq %rax, %rax
    callq curl_easy_setopt

    # NOPROGRESS
    movq (%rsp), %rdi
    movl $43, %esi
    movl $0x1, %edx
    xorq %rax, %rax
    callq curl_easy_setopt

    # WRITEFUNCTION
    movl $.writecb, %edx
    movl $20011, %esi
    movq (%rsp), %rdi
    xorq %rax, %rax
    callq curl_easy_setopt

    # PERFORM
    movq (%rsp), %rdi
    callq curl_easy_perform

    xorq %rax, %rax
    leave
    rep; retq

main: 
    pushq %rbp
    movq %rsp, %rbp
    subq $0x8, %rsp

    callq curl_easy_init

    pushq %rax # save the curl handle
    movq %rax, %rdi
    callq .startcrawler

    movq (%rsp), %rdi
    callq curl_easy_cleanup

    movq $0x0, %rdi
    leave
    rep; retq
