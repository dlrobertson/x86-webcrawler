PROJECT_NAME=crawler
TDIR=./target

all:
	as --64 -o ./crawler.o crawler.s
	/bin/ld -o $(TDIR)/$(PROJECT_NAME) -m elf_x86_64 -L/usr/lib64  -dynamic-linker /lib64/ld-linux-x86-64.so.2 \
	/usr/lib64/crt1.o /usr/lib64/crti.o -lc ./crawler.o /usr/lib64/crtn.o -lcurl

run: all
	$(TDIR)/$(PROJECT_NAME)
